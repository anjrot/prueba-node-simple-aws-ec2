const express = require("express");
const app = express();
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(express.static(path.join(__dirname, "public")));

app.get("/", async (req, res) => {
  res.send(
    "Estamos jugando primero con Node.js, desde el deploy de Gitlab y ahora lo esta viendo Eliezer!!!! \n Probando un Nuevo Pipeline"
  );
});
app.get("/prueba", async (req, res) => {
  res.send("Esta es la pagina de pruebas");
});

app.listen(3000, () => {
  console.log("Aplicación levantada en el 3000");
});
