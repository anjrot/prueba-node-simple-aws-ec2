#!/bin/bash

# any future command that fails will exit the script
set -e
echo "Estoy dentro de la instancia de EC2"

cd apps/ && rm -rf prueba-node-simple-aws-ec2/

git clone https://gitlab.com/anjrot/prueba-node-simple-aws-ec2.git

cd prueba-node-simple-aws-ec2/

npm install

pm2 restart 0

echo "Listo todos los procesos"


exit