#!/bin/bash

# any future command that fails will exit the script
set -e

mkdir /root/.ssh && touch /root/.ssh/anjrot.pem

echo -e "$PRIVATE_KEY" > /root/.ssh/anjrot.pem
chmod 400 /root/.ssh/anjrot.pem

echo "Muestra el anjrot.pem"
cat /root/.ssh/anjrot.pem


touch /root/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> /root/.ssh/config

echo "Muestra el config"
cat /root/.ssh/config

ls -la

ssh -i /root/.ssh/anjrot.pem ubuntu@ec2-34-227-157-74.compute-1.amazonaws.com 'bash -s' < ./terminal/dentroEC2.sh

echo "Completado el proceso"
echo "Chao..."
